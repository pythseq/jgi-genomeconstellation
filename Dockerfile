FROM python:3.7-slim
MAINTAINER zhongwang@lbl.gov
USER root
WORKDIR /app
ADD . /app
RUN pip install --trusted-host pypi.python.org -r requirements.txt
EXPOSE 80 443
ENV NAME genome-constellation-classifier
CMD ["gunicorn", "-b", ":80", "-b", ":443", "-t", "600", "app"]
