#!/usr/bin/env python
import sys
import json
from pprint import pprint

def usage():
    print('To report counts with value < ANI : %s LINK-JSON ANI' % sys.argv[0])
    sys.exit(1)

if len(sys.argv) < 3:
    usage()

ifile = sys.argv[1]
ani = int(sys.argv[2])

print("in file: %s; ani=%d\n" % (ifile, ani))

with open(ifile) as fh:
    obj = json.load(fh)

    links = None
    if 'links' in obj:
        links = obj['links']
    elif type(obj) == list:
        links = obj

    if links:
        print('links : %d' % len(links))
        cnt = 0
        for link in links:
            #print(link)
            #exit(1)
            if link['value'] < ani:
                cnt += 1

        print('there %d items with value < %d' % (cnt, ani))

    else:
        print('%s is not a link file' % ifile)
        sys.exit(1)
