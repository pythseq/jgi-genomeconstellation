#!/bin/bash
set -e
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
USAGE="USAGE: $0 <fasta_dir> <file_name_pattern> <gc_sketch_path> <output_prefix>\
	  \nExample: $0 ~/Fasta_dir \"*.fa\" ~/ref my_sketches"

# user inputs
FOLDER=$1
MASK=$2
REFERENCE_SKETCH_PATH=$3
PREFIX=$4
TOOLS=$(realpath $0)
TOOLS=$(dirname $TOOLS)

# config values
REF_SKETCH_NAME="ref_06102017.fp"
REF_FILES_LIST="ref_06102017.list"
SKETCH_SIZE=10000
MAX_PARALLEL_PROC=8
SIMILARITY_THRESHOLD=70
SELF_SIM_MATRIX_NAME=$PREFIX.SELF.dist.mx
REF_SIM_MATRIX_NAME=$PREFIX.REF.dist.mx
SIM_MATRIX_NAME=$PREFIX.sim.mx
ANNOTATION_FILE_BASENAME=$PREFIX
ANNOTATION_FILE=$ANNOTATION_FILE_BASENAME.csv
DISTANCE_THRESHOLD=0.75
SKETCH_NAME=$PREFIX.fp
LIST_NAME=$PREFIX.list

if ! command -v mash >/dev/null; then
	echo "Mash not found. Please install mash and specify in PATH."
	exit -1
fi


if 	[[ -z $FOLDER ]] || \
	[[ -z $MASK ]] || \
	[[ -z $REFERENCE_SKETCH_PATH ]] || \
	[[ -z $PREFIX ]]; then		
		echo -e $USAGE
		exit -1
fi

echo "Generating list of files..."
ls $FOLDER/$MASK > $LIST_NAME

echo "Generating sketches..."
$TOOLS/jgi_gc -i $LIST_NAME --outFP $SKETCH_NAME

echo "Generating SELF distance matrix..."
$TOOLS/jgi_gc 	--minANI $SIMILARITY_THRESHOLD \
		--query $SKETCH_NAME --self --outGC $SELF_SIM_MATRIX_NAME

echo "Generating REF distance matrix..."
$TOOLS/jgi_gc 	--minANI $SIMILARITY_THRESHOLD \
		--query $SKETCH_NAME \
		--target $REFERENCE_SKETCH_PATH/$REF_SKETCH_NAME \
		--outGC $REF_SIM_MATRIX_NAME

echo "Generating annotation file... "
rm -rf $ANNOTATION_FILE_BASENAME.csv
cat $LIST_NAME | xargs -Ix basename x | awk '{print $1"\t0\t"$1}' > $ANNOTATION_FILE

awk -f search_and_replace_REF_first_col.awk \
	$ANNOTATION_FILE \
	$REF_SIM_MATRIX_NAME > dummy.1

awk -f search_and_replace_REF_second_col.awk \
	$REFERENCE_SKETCH_PATH/$REF_FILES_LIST \
	dummy.1 > dummy.2

awk -f search_and_replace_SELF.awk \
	$ANNOTATION_FILE \
	$SELF_SIM_MATRIX_NAME > $SIM_MATRIX_NAME

awk --posix -f reformat_names_gc.awk dummy.2 >> $SIM_MATRIX_NAME

echo "Generating combined json..."
python $SCRIPT_DIR/create_json.py $ANNOTATION_FILE_BASENAME.csv $SIM_MATRIX_NAME

rm -f $ANNOTATION_FILE_BASENAME.csv
rm -f $SIM_MATRIX_NAME
rm -f $REF_SIM_MATRIX_NAME
rm -f $SELF_SIM_MATRIX_NAME
rm -f $SKETCH_NAME
rm dummy.1 dummy.2

