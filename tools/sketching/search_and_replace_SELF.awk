# Usage: gawk -f search_and_replace.awk LABELS.tsv MYFILE.tsv
# LABELS.tsv: Name \t NewName
# MYFILE.tsv: Name1 Name2 blah1 blah2 blah3 ...
# OUTPUT.tsv: NewName1 NewName2 blah1 blah2 blah3 ...

BEGIN {FS=OFS="\t"; count=1;}
{
    if(NR==FNR){
		a[count]=$1;
		count++;
		next;
    }

    if($1 in a) printf a[$1]"\t";
    else printf $1"\t";

    if($2 in a) printf a[$2]"\t";
    else printf $2"\t";

    print $3;
}
