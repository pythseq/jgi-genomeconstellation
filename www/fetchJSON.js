/**
 * The concurrent js worker
 * use jQuery to fetch server-side file ...
 */

// in order to load jQuery, DOM object is needed
importScripts("workerFakeDOM.js");  // the fake DOM object
importScripts('https://code.jquery.com/jquery-2.1.4.min.js');

// the worker's event handler (handles postMessage event)
self.addEventListener('message', function(e) {
    // worker task ..
    $.ajax({
        type:"GET",
        dataType: "html",
        url: e.data,
        async: true,
        dataType: "json",
        success: function(data) {
                //console.log("fetchJSON SUCCESS")
                //console.dir(data)
                self.postMessage({"file_name":e.data, "data":data});
            },
        error: function(jqXHR, textStatus, errorThrown) {
                console.log("fetchJSON ERROR")
                console.dir(jqXHR)
                //console.log(textStatus)
                //console.log(errorThrown)
                self.postMessage({"file_name":e.data, 'status': jqXHR.status, 'errors': 'fetch failed'});
            },
    })
}, false);
